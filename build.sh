#!/bin/bash

rm robot.o > /dev/null 2>/dev/null
rm emojicode-robot > /dev/null 2>/dev/null
rm emojicode_hal.o > /dev/null 2>/dev/null

arm-frc2019-linux-gnueabi-g++ -Iinclude emojicode_hal.cpp -c -fpermissive
emojicodec robot.emojic -c

arm-frc2019-linux-gnueabi-g++ robot.o emojicode_hal.o packages/s/libs.a packages/runtime/libruntime.a -pthread -Llib -lwpiHal -lwpiutil -lvisa -lRoboRIO_FRC_ChipObject -lNiRioSrv -lniriosession -lniriodevenum -lNiFpgaLv -lNiFpga -lFRC_NetworkCommunication -o emojicode-robot
