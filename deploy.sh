#!/bin/bash

./build.sh

ssh admin@manirio '. /etc/profile.d/natinst-path.sh; /usr/local/frc/bin/frcKillRobot.sh -t 2> /dev/null; rm -f /home/lvuser/emojicode-robot'
scp emojicode-robot lvuser@manirio:~/emojicode-robot
scp robotCommand lvuser@manirio:~/robotCommand
ssh admin@manirio 'chmod +x /home/lvuser/robotCommand; chown lvuser /home/lvuser/robotCommand; \
                   chmod +x /home/lvuser/emojicode-robot; chown lvuser /home/lvuser/emojicode-robot; \
                   sync; ldconfig; sleep 2; . /etc/profile.d/natinst-path.sh; /usr/local/frc/bin/frcKillRobot.sh -t -r'
