#include <hal/HAL.h>
#include <runtime/Runtime.h>
#include <s/String.h>
// #include <s/Error.h>
#include <iostream>

extern "C" void EC_HAL_Initialize(runtime::ClassInfo*, runtime::Integer timeout, runtime::Integer mode) {
    HAL_Initialize((int32_t) timeout, (int32_t) mode);
}

extern "C" void EC_HAL_ObserveUserProgramStarting(runtime::ClassInfo*) {
    HAL_ObserveUserProgramStarting();
    std::cout << "************** USER PROGRAM STARTING **************\n";
}

extern "C" void EC_HAL_ReportLanguage(runtime::ClassInfo*, s::String* name) {
    HAL_Report(HALUsageReporting::kResourceType_Language, 7, 0, name->stdString().c_str());
}

extern "C" runtime::Integer EC_HAL_InitializePWMChannel(runtime::ClassInfo* classInfo, runtime::Integer channel, runtime::Raiser* raiser) {
    if(!HAL_CheckPWMChannel((int32_t) channel)) {
        // raiser->raise(new s::Error("Invalid PWM Channel"), "");
        return -1;
    }

    int status;
    auto handle = HAL_InitializePWMPort(HAL_GetPort((int32_t) channel), &status);

    if(status != 0) {
        // raiser->raise(new s::Error(HAL_GetErrorMessage(status)), "");
        return -1;
    }

    return (int64_t) handle;
}

extern "C" void EC_HAL_DeInitPWMPort(runtime::ClassInfo*, runtime::Integer handle, runtime::Raiser* raiser) {
    int status;
    HAL_SetPWMDisabled(handle, &status);
    if(status != 0) {
        // raiser->raise(new s::Error(HAL_GetErrorMessage(status)), "");
        return;
    }
    HAL_FreePWMPort(handle, &status);
    if(status != 0) {
        // raiser->raise(new s::Error(HAL_GetErrorMessage(status)), "");
        return;
    }
}

extern "C" void EC_HAL_SetPWMRaw(runtime::ClassInfo*, runtime::Integer handle, runtime::Integer value) {
    int status;
    HAL_SetPWMRaw(handle, value, &status);
}

extern "C" runtime::Integer EC_HAL_GetPWMRaw(runtime::ClassInfo*, runtime::Integer handle) {
    int status;
    return HAL_GetPWMRaw(handle, &status);
}

extern "C" void EC_HAL_SetPWMSpeed(runtime::ClassInfo*, runtime::Integer handle, runtime::Real speed) {
    int status;
    HAL_SetPWMSpeed(handle, speed, &status);
}

extern "C" runtime::Real EC_HAL_GetPWMSpeed(runtime::ClassInfo*, runtime::Integer handle) {
    int status;
    return HAL_GetPWMSpeed(handle, &status);
}

extern "C" void EC_HAL_SetPWMConfig(runtime::ClassInfo*, runtime::Integer handle, runtime::Real max, runtime::Real deadband_max, runtime::Real center, runtime::Real deadband_min, runtime::Real min) {
    int status;
    HAL_SetPWMConfig(handle, max, deadband_max, center, deadband_min, min, &status);
}

extern "C" void EC_HAL_LatchPWMZero(runtime::ClassInfo*, runtime::Integer handle) {
    int status;
    HAL_LatchPWMZero(handle, &status);
}