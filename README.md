# FRC Robot Code using Emojicode

This monstrosity of a project speaks to the nature of human endurance towards the stupidest of goals

## Preparation
If you must compile this for yourself, be prepared to put in some effort

The [emojicode](https://github.com/emojicode/emojicode) compiler only (officially) supports compiling to the host architecture. As such, the first step in reproducing this is to roll up your sleeves and modify some compilers

Before starting this process, be sure to install the official FRC C++ toolchain, it will be required

1. Clone the emojicode repository linked above
2. Open `Compiler/Generation/CodeGenerator.cpp`, navigate to line 53 and replace `targetTriple` with the following:

`std::string targetTriple = "arm-unknown-linux-gnueabi";`

3. Save that file, and open `Compiler/CLI/main.cpp`, navigate to line 61, and replace `options.linker()` with the following

`"arm-frc2019-linux-gnueabi-g++"`

Following that, replace `options.ar()` on line 64 with `"arm-frc2019-linux-gnueabi-ar"`.

4. Go to the root of the repository, and follow the compilation instructions on the wiki. Once compiled, run `dist.py` in the root of the repository
5. That last will have created a new directory containing the compiler and core libs. Note that this installation is not general purpose. In replacing `targetTriple`, this will only function to create roboRIO executables, and will not work with the libraries as created in that directory

With that in mind, copy `emojicodec` to some place on your path, for instance `/usr/local/bin/emojicodec`.

## Usage
1. Clone this repository
2. Navigate into the repository, and replace `manirio` with the IP of your roborio in `deploy.sh`
3. Edit `robot.emojic` to add all sorts of game-breaking, IIC winning features to your new codebase
4. When you want to deploy to the robot, run `deploy.sh`.

If you'd like to access more features similar to what is provided in wpilib, modify emojicode_hal.cpp to add FFI compatible wrappers of the HAL functions, and add their declarations to the `🤖` class created at the top of the file

## Why?
Why not.